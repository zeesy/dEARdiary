module.exports = {
  globDirectory: 'static/',
  globPatterns: [
    '**/*.{png,ico,svg}',
  ],
  swDest: 'static/sw.js',
  skipWaiting: true,
  clientsClaim: true,
};
