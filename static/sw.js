/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js");

workbox.skipWaiting();
workbox.clientsClaim();

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "favicon/android-icon-144x144.png",
    "revision": "f709acabcea89f0e43add0d94761902a"
  },
  {
    "url": "favicon/android-icon-192x192.png",
    "revision": "e71d433a37e1be2e6485d6e8fa174e4d"
  },
  {
    "url": "favicon/android-icon-36x36.png",
    "revision": "d6606ee93cf02bd0284383c3cf3cd05d"
  },
  {
    "url": "favicon/android-icon-48x48.png",
    "revision": "b08ddc4fe844ec4298ae9d30cccc919f"
  },
  {
    "url": "favicon/android-icon-72x72.png",
    "revision": "959a71fac4f7902a3c118b0bcf992680"
  },
  {
    "url": "favicon/android-icon-96x96.png",
    "revision": "c8664549396735060e1c8a426b8a9583"
  },
  {
    "url": "favicon/apple-icon-114x114.png",
    "revision": "054a61097b5377bc0cf426c7edcf6e7e"
  },
  {
    "url": "favicon/apple-icon-120x120.png",
    "revision": "77b38b3a98bab92e8c9994faa8e4d209"
  },
  {
    "url": "favicon/apple-icon-144x144.png",
    "revision": "f709acabcea89f0e43add0d94761902a"
  },
  {
    "url": "favicon/apple-icon-152x152.png",
    "revision": "218e9946d6f088b1a9f840abf075d3fe"
  },
  {
    "url": "favicon/apple-icon-180x180.png",
    "revision": "0c1d33fb790037304b1e62c4fe24656e"
  },
  {
    "url": "favicon/apple-icon-57x57.png",
    "revision": "c8ce9f5656f27c3a37e5c6480e9a2ca1"
  },
  {
    "url": "favicon/apple-icon-60x60.png",
    "revision": "c373adaee96f930c8d030a7f51ac3111"
  },
  {
    "url": "favicon/apple-icon-72x72.png",
    "revision": "959a71fac4f7902a3c118b0bcf992680"
  },
  {
    "url": "favicon/apple-icon-76x76.png",
    "revision": "07263043b82bb3014032d31ad1b56636"
  },
  {
    "url": "favicon/apple-icon-precomposed.png",
    "revision": "54cb0650d0b584fe220c00e5a7b48e37"
  },
  {
    "url": "favicon/apple-icon.png",
    "revision": "54cb0650d0b584fe220c00e5a7b48e37"
  },
  {
    "url": "favicon/favicon-16x16.png",
    "revision": "3852075f75776f51a96a12c28d6083f7"
  },
  {
    "url": "favicon/favicon-32x32.png",
    "revision": "dc512d4f590088a03e6d4c6ac9a3bd42"
  },
  {
    "url": "favicon/favicon-96x96.png",
    "revision": "c8664549396735060e1c8a426b8a9583"
  },
  {
    "url": "favicon/favicon.ico",
    "revision": "a2a90a0239836528e1f5dab80f25fd74"
  },
  {
    "url": "favicon/ms-icon-144x144.png",
    "revision": "f709acabcea89f0e43add0d94761902a"
  },
  {
    "url": "favicon/ms-icon-150x150.png",
    "revision": "c590da9432e432be394275361a5530ea"
  },
  {
    "url": "favicon/ms-icon-310x310.png",
    "revision": "4ec7fe53af176c73d02eb2c44ab0d07e"
  },
  {
    "url": "favicon/ms-icon-70x70.png",
    "revision": "264a76bd697b2d5682108d13a0263b64"
  },
  {
    "url": "images/loaders/droplet_loader.svg",
    "revision": "233d0ed693adc3a760a80289748832b1"
  },
  {
    "url": "images/loaders/ellipsis_loader.svg",
    "revision": "c37112c2a3aefe8d700c5f2b14856f57"
  },
  {
    "url": "images/touch/homescreen144.png",
    "revision": "d88f3a6b5fd5cfe29908885f86242638"
  },
  {
    "url": "images/touch/homescreen144.svg",
    "revision": "9183685b4406694bc54ff97be5a2c237"
  },
  {
    "url": "images/touch/homescreen168.png",
    "revision": "b5df9935d5a6aa3ca967406eaae90ebf"
  },
  {
    "url": "images/touch/homescreen168.svg",
    "revision": "280003dc5bddba3187b8b6a5b3de89bf"
  },
  {
    "url": "images/touch/homescreen198.png",
    "revision": "2c9a213bdec0fdcb1a9f5af211385de6"
  },
  {
    "url": "images/touch/homescreen198.svg",
    "revision": "c5dfc5192197ab49c5d047947fe883bf"
  },
  {
    "url": "images/touch/homescreen48.png",
    "revision": "82c09b7e233068a61ff8fe7dfd8f349b"
  },
  {
    "url": "images/touch/homescreen48.svg",
    "revision": "f880da6814ba1b10706f8761e7875dec"
  },
  {
    "url": "images/touch/homescreen512.png",
    "revision": "57602d2fe731a6d0c41fdbff70c182a0"
  },
  {
    "url": "images/touch/homescreen512.svg",
    "revision": "8f35a65e35839dc30870023e491059e7"
  },
  {
    "url": "images/touch/homescreen72.png",
    "revision": "93199f88f618a09043e0fa8d6c811af5"
  },
  {
    "url": "images/touch/homescreen72.svg",
    "revision": "a33ccb2f3a60a294b64e8fc5dd574ed9"
  },
  {
    "url": "images/touch/homescreen96.png",
    "revision": "fef322f29da2731c2624ed079c3af628"
  },
  {
    "url": "images/touch/homescreen96.svg",
    "revision": "ccb3c1930e1b531cefd384d1a036a5b7"
  },
  {
    "url": "splashscreens/ipad_splash.png",
    "revision": "65d3cd0e57cf52914d83b6e79f44755e"
  },
  {
    "url": "splashscreens/ipadpro1_splash.png",
    "revision": "138de7f3659ac9ac886761964fed2fe4"
  },
  {
    "url": "splashscreens/ipadpro2_splash.png",
    "revision": "740b8bb1a1b4ab2f65b821c73b6799ae"
  },
  {
    "url": "splashscreens/ipadpro3_splash.png",
    "revision": "fdc8eeb2d28cb3f536507747f3c9b404"
  },
  {
    "url": "splashscreens/iphone5_splash.png",
    "revision": "096e35869e601383119d3544a78f9209"
  },
  {
    "url": "splashscreens/iphone6_splash.png",
    "revision": "db5a1dbd980018aa2a8a61284e5ab9d3"
  },
  {
    "url": "splashscreens/iphoneplus_splash.png",
    "revision": "5ca547edc1f3f8525a7980a43be7549a"
  },
  {
    "url": "splashscreens/iphonex_splash.png",
    "revision": "4bfcd23e2ea513f16566231f3c292cbb"
  },
  {
    "url": "splashscreens/iphonexr_splash.png",
    "revision": "9ee9885cfd874265eaf0a467c8314eb7"
  },
  {
    "url": "splashscreens/iphonexsmax_splash.png",
    "revision": "59945c94fb4ddd5a477cd9dae058c8c2"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
