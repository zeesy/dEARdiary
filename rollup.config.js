import resolve from 'rollup-plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import commonjs from 'rollup-plugin-commonjs';
import litcss from 'rollup-plugin-lit-css';

export default {
  input: 'src/main.js',
  output: {
    file: 'static/main.js',
    sourcemap: true,
    format: 'es',
  },
  plugins: [
    litcss(),
    resolve(),
    terser(),
    commonjs(),
  ],
};
