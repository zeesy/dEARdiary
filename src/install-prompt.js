const btnAdd = document.getElementById('addbutton');
const btnInstall = document.getElementById('installbutton');

// INSTALL BOX --- what is this and why doesn't it work?
let deferredPrompt;

window.addEventListener('beforeinstallprompt', e => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI notify the user they can add to home screen
  btnAdd.style.display = 'block';
});

btnAdd.addEventListener('click', e => {
  // hide our user interface that shows our A2HS button
  btnAdd.style.display = 'none';
  // Show the prompt
  deferredPrompt.prompt();
  // Wait for the user to respond to the prompt
  deferredPrompt.userChoice
    .then(choiceResult => {
      if (choiceResult.outcome === 'accepted') {
        // eslint-disable-next-line no-console
        console.log('User accepted the A2HS prompt');
      } else {
        // eslint-disable-next-line no-console
        console.log('User dismissed the A2HS prompt');
      }
      deferredPrompt = null;
    });
});

btnInstall.addEventListener('click', () => {
  // Update the install UI to remove the install button
  document.querySelector('#install-button').disabled = true;
  // Show the modal add to home screen dialog
  installPromptEvent.prompt();
  // Wait for the user to respond to the prompt
  installPromptEvent.userChoice.then(choice => {
    if (choice.outcome === 'accepted') {
      // eslint-disable-next-line no-console
      console.log('User accepted the A2HS prompt');
    } else {
      // eslint-disable-next-line no-console
      console.log('User dismissed the A2HS prompt');
    }
    // Clear the saved prompt since it can't be used again
    installPromptEvent = null;
  });
});

let installPromptEvent;

window.addEventListener('beforeinstallprompt', event => {
  // Prevent Chrome <= 67 from automatically showing the prompt
  event.preventDefault();
  // Stash the event so it can be triggered later.
  installPromptEvent = event;
  // Update the install UI to notify the user app can be installed
  document.querySelector('#install-button').disabled = false;
});


window.addEventListener('beforeinstallprompt', event => {
  // eslint-disable-next-line no-console
  console.log('will prompt');
  event.prompt(); // shows dialog
});

window.addEventListener('appinstalled', evt => {
  // TODO: send analytics that the app was installed.
  // Example google analytics code:
  // app.logEvent('a2hs', 'installed');
});
