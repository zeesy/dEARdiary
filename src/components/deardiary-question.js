import { LitElement, html, css } from 'lit-element';
import { PageMixin } from './page-mixin';
import buttonLinkStyles from '../shared-styles/button-link.css';
import pageStyles from '../shared-styles/page-styles.css';
import propOr from 'crocks/helpers/propOr';
import { readValue, writeValue } from '../storage.js';
import { writeSingleEntry } from '../lib/db.js';
import { handleAsJson } from '../lib/network.js';

// BELOW TO BE REORGANIZED BASED ON database, UI, network, utilities js modules

const updateDb = ({ passage, question }) => answer =>
  writeSingleEntry({ date: new Date(), answer, passage, question });

const getResult = propOr('', 'result');

const fetchQuestion = async ({ passage, question }) => {
  if (!question) throw new Error('No Question');
  if (!passage) throw new Error('No Passage');
  // location.origin e.g. https://dEARdiary.zeesyapp.com
  const url = new URL('/ask', location.origin);
  url.search = new URLSearchParams({ paragraph: passage, question });
  return fetch(url);
};

// TODO: render some useful UI to the user, like a message to add a question or whatev.
const handleError = error =>
  console.error(error); // eslint-disable-line no-console

class DeardiaryQuestion extends PageMixin(LitElement) {
  static get is() {
    return 'deardiary-question';
  }

  static get properties() {
    return {
      question: { type: String },
      passage: { type: String },
    };
  }

  static get styles() {
    return [
      pageStyles,
      buttonLinkStyles,
      css`
        #submit {
          margin-top: 14px;
        }
      `,
    ];
  }

  constructor() {
    super();
    this.fireSubmitEvent = this.fireSubmitEvent.bind(this);
  }

  render() {
    return html`
      <input id="question-input" ?disabled="${!this.active}"
          placeholder="What Do You Want to Know?"
          type="text"
          aria-labelledby="question-heading"
          name="question"
          .value="${this.question || ''}"
          @keyup="${this.onKeyup}">
      <button id="submit" class="button-link" @click="${this.submit}">Press for Answer</button>
    `;
  }

  onActiveChanged(active) {
    if (!active) return this.$('question-input').blur();
    this.question = readValue('question-input');
    this.passage = readValue('passage-input');
    this.$('question-input').focus();
  }

  onKeyup(event) {
    writeValue('question-input', event.target.value);
  }

  /* Fetches an NLP response then updates the UI */
  submit(event) {
    const passage = readValue('passage-input');
    const question = readValue('question-input');
    return fetchQuestion({ passage, question })
      .then(handleAsJson)
      .then(getResult)
    // Side effect: update the database with { answer, passage, question };
      .then(updateDb({ passage, question }))
    // Side effect: update document
      .then(this.fireSubmitEvent)
      .catch(handleError);
  }

  /* Displays the response in the document. */
  fireSubmitEvent({ answer }) {
    this.dispatchEvent(new CustomEvent('answer-submitted', { detail: answer }));
  }

  async reset() {
    await this.updateComplete;
    this.$('question-input').value = '';
  }
}

customElements.define(DeardiaryQuestion.is, DeardiaryQuestion);
