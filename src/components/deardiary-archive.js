import { LitElement, html, css } from 'lit-element';
import { formatWithOptions } from 'date-fns/fp';
import compose from 'crocks/helpers/compose';
import { PageMixin } from './page-mixin';
import { getAllEntries } from '../lib/db.js';
import buttonLinkStyles from '../shared-styles/button-link.css';
import pageStyles from '../shared-styles/page-styles.css';

const newDate = x => new Date(x);

const formatDateWithOptions = formatWithOptions({
  awareOfUnicodeTokens: true,
}, 'd MMMM, yyyy, h:mm a');

/**
 * Formats an ISO date string as 'd MMMM, yyyy, h:mm a'
 * Ben's suggested method for date format
 * @type {Function}
 * @param {String} isoString ISO Date String
 * @return {String} Date formatted as 'd MMMM, yyyy, h:mm a'
 */
const prettyDate = compose(formatDateWithOptions, newDate);

const byISODate = (a, b) => {
  const aDate = new Date(a.date).getTime();
  const bDate = new Date(b.date).getTime();
  return (
      aDate > bDate ? -1
    : aDate < bDate ? 1
    : 0
  );
};

const entryTemplate = ({ date, passage, question, answer }) => html`
  <li>
    <details>
      <summary><time>${prettyDate(date)}</time></summary>
      <dl>
        <dt>Passage</dt>
        <dd>${passage}</dd>
        <dt>Question</dt>
        <dd>${question}</dd>
        <dt>Answer</dt>
        <dd>${answer}</dd>
      </dl>
    </details>
  </li>`;

class DeardiaryArchive extends PageMixin(LitElement) {
  static get is() {
    return 'deardiary-archive';
  }

  static get properties() {
    return {
      entries: { type: Array },
    };
  }

  static get styles() {
    return [
      pageStyles,
      buttonLinkStyles,
      css`
        li {
          width: calc(100vw - var(--page-padding) * 2)
        }

        ol {
          padding: 0;
          color: #ffffff;
          list-style-type: none;
        }

        summary {
          font-size: 22px;
          padding: 10px 0;
        }

        dt {
          font-family: var(--header-font);
        }

        dl {
            font-family: 'Anonymous Pro', monospace;
        }
      `,
    ];
  }

  constructor() {
    super();
    this.entries = [];
  }

  render() {
    return html`
      <h1>Archive</h1>

      <ol>
        ${this.entries.sort(byISODate).map(entryTemplate)}
      </ol>

      <br/>

      <a id="files-button" class="button-link" href="/archivefiles">Manage Your Files</a>

      <br/>

      <a id="home-button" class="button-link" href="./">Home</a>`;
  }

  onActiveChanged(active) {
    if (active) this.getEntries();
  }

  async getEntries() {
    this.entries = await getAllEntries();
    this.entry = (new URLSearchParams(location.search.substring(1)))
      .get('date');
  }
}

customElements.define(DeardiaryArchive.is, DeardiaryArchive);
