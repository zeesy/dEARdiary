export const storeOnInput = key => ({ target: { value } }) => writeValue(key, value);
export const readValue = key => localStorage.getItem(key);
export const writeValue = (key, val) => localStorage.setItem(key, val);
