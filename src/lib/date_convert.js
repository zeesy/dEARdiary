import { format } from 'date-fns';

// Format the date:
export default function dateResult() {
  format(new Date(), 'd MMMM, yyyy h:m a');
}

// sourcemap = true;
