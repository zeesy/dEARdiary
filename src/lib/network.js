

/* handles a Response as JSON */
export const handleAsJson = response => response.json();
