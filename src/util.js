export const constant = x => () => x;

// date in local time constructor
export function CreateDateAsUTC(date) {
  return new Date(
    Date.UTC(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes(),
      date.getSeconds())
  );
  // return format(date, new Date(), "DD MMMM, YYYY, h:m a")
}
