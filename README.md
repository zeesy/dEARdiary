In-development. Reflection therapy through Natural Language Programming.

Built on top of the AllenNLP Machine Comprehension demo.


Developing
========

To get the development server up run:

`docker-compose up`

It'll be available at `http://localhost:8888`

Rebuild Javascript with 

`npm run build`